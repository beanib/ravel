<?php
/**
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU 
 * General Public License as published by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write 
 * to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * @package    Ravel
 * @subpackage Functions
 * @author     Tung Do, <ttsondo@gmail.com>
 * @author     Justin Tadlock, <justin@justintadlock.com>
 * @copyright  Copyright (c) 2014, Tung Do, Justin Tadlock
 * @link       http://themehybrid.com/themes/ravel
 * @license    http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/* Get the template directory and make sure it has a trailing slash. */
$ravel_dir = trailingslashit( get_template_directory() );

/* Load the Hybrid Core framework and launch it. */
require_once( $ravel_dir . 'library/hybrid.php' );
new Hybrid();

/* Load theme-specific files. */
require_once( $ravel_dir . 'inc/custom-header.php' );

// Include the custom-meta-boxes.php framework file.
require_once( 'inc/Custom-Meta-Boxes/custom-meta-boxes.php' );

/* Set up the theme early. */
add_action( 'after_setup_theme', 'ravel_theme_setup', 5 );


function ravel_scripts_styles() {
//	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' );
	//wp_enqueue_style( 'jquery.fancybox.css', get_template_directory_uri() . '/css/jquery.fancybox.css', array( ), false, 'all' );
  	//wp_enqueue_script( 'jquery.fancybox.js', get_template_directory_uri() . '/js/jquery.fancybox.js', array( ), false, 'all' );
 	wp_enqueue_style( 'jquery.fancybox.css', get_template_directory_uri() . '/css/lightbox.css', array( ), false, 'all' );
  	wp_enqueue_script( 'jquery.fancybox.js', get_template_directory_uri() . '/js/lightbox.js', array( ), false, 'all' );
 
 }
add_action( 'wp_enqueue_scripts', 'ravel_scripts_styles' );

/**
 * The theme setup function.  This function sets up support for various WordPress and framework functionality.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function ravel_theme_setup() {

	/* Load files. */
	require_once( trailingslashit( get_template_directory() ) . 'inc/ravel.php'     );
	require_once( trailingslashit( get_template_directory() ) . 'inc/customize.php' );

	/* Load widgets. */
	add_theme_support( 'hybrid-core-widgets' );

	/* Theme layouts. */
	add_theme_support( 
		'theme-layouts',
		array( '1c' => __( '1 Column', 'ravel' ) ),
		array( 'customize' => false, 'post_meta' => false )
	);

	/* Load stylesheets. */
	add_theme_support(
		'hybrid-core-styles',
		array( 'ravel-fonts', 'ravel-mediaelement', 'ravel-wp-mediaelement', 'parent', 'style' )
	);

	/* Enable custom template hierarchy. */
	add_theme_support( 'hybrid-core-template-hierarchy' );

	/* The best thumbnail/image script ever. */
	add_theme_support( 'get-the-image' );

	/* Pagination. */
	add_theme_support( 'loop-pagination' );

	/* Nicer [gallery] shortcode implementation. */
	add_theme_support( 'cleaner-gallery' );

	/* Better captions for themes to style. */
	add_theme_support( 'cleaner-caption' );
	
	/* If support for Cleaner Gallery is removed, remove default gallery style as well and use ambience's gallery CSS. */
	if( !current_theme_supports('cleaner-gallery') ) {
		/* Remove default gallery inline CSS */
		add_filter( 'use_default_gallery_style', '__return_false' );
		}

	/* Automatically add feed links to <head>. */
	add_theme_support( 'automatic-feed-links' );

	/* Post formats. */
	add_theme_support( 
		'post-formats', 
		array( 'aside', 'audio', 'chat', 'image', 'gallery', 'link', 'quote', 'status', 'video' ) 
	);

	/* Editor styles. */
	add_editor_style( ravel_get_editor_styles() );

	/* Handle content width for embeds and images. */
	// Note: this is the largest size based on the theme's various layouts.
	hybrid_set_content_width( 728 );
}

/*-----------------------------------------*/
/* Cargar Panel de Opciones
/*-----------------------------------------*/
if ( !function_exists( 'optionsframework_init' ) ) {
  define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
  require_once dirname( __FILE__ ) . '/inc/options-framework.php';
}

/* Creating a custom meta box */
function cmb_ravel_metaboxes( array $meta_boxes ) {
	$fields = array(
		array( 	'id' => 'fecha', 
				'name' => 'Fecha',
				'type' => 'date', 'cols' => 2  ),
		array( 	'id' => 'indice', 
				'name' => 'Indice',
				'type' => 'text', 'cols' => 3 
			),
		array( 	'id' => 'minutos',
				'name' => 'Minutos',
				'type' => 'text', 'cols' => 3 
			),
		array( 	'id' => 'alerta', 
				'name' => 'Alerta',
				'type' => 'select',
				'options' => array(
			        '1' => 'Naranja - Moderado',
			        '2' => 'Roja - Alto',
			        '3' => 'Morada - Muy Alto',
			       	'4' => 'Violeta - Estrema',
    			), 'cols' => 4 
			),
	);
	$meta_boxes[] = array(
		'title' => 'Valores Críticos de UV-B medidos en Tarija:',
		'pages' => 'post',
		'fields' => $fields
	);
	return $meta_boxes;
}

add_filter( 'cmb_meta_boxes', 'cmb_ravel_metaboxes' );

/* Mi funcion para obtener el color de la alerta */
function set_color_alerta( $alerta ){
	//$kolor = "#e36c0a";
	switch ($alerta) {
		case '2':
			$kolor = "#ff0000";
			break;
		case '3':
			$kolor = "#cc00cc";
			break;
		case '4':
			$kolor = "#7030a0";
			break;
		default:
			$kolor = "#e36c0a";
			break;
	}
	return $kolor;
}

/* Mi funcion para obtener la informacion de la alerta */
function get_info_alerta( $alerta ){
	//$info = "#e36c0a";
	switch ($alerta) {
		case '2':
			$info = "Roja - Alto";
			break;
		case '3':
			$info = "Morada - Muy Alto";
			break;
		case '4':
			$info = "Violeta - Estremo";
			break;
		default:
			$info = "Naranja - Moderado";;
	}
	return $info;
}

/* Mostrar todos los post de una categoría en Wordpress */
function show_category_posts( $atts ){
        extract(shortcode_atts(array(
                'cat'=> '',
                'mostrar' => ''
        ), $atts));
        query_posts('cat='.$cat.'&orderby=date&order=DES&posts_per_page=-1'.$mostrar);
        if ( have_posts() ){
                $content = '<tbody>';
                while ( have_posts() ){
                    the_post();
                  	//Cambiando la fecha la formato dia, mes, anio.
                  	$hoy= date('d\/m\/Y', strtotime('now')) ;
                    $fecha = date('d\/m\/Y', strtotime( get_post_meta( get_the_id(), 'fecha', true ) )) ;
                    $difer = $hoy - $fecha;
                    //0 = ese dia, -1= un dias mas, -2 = dos dias mas
                    if ($difer >= -1) {
	                    echo '<tr>';
	                    echo '<td>'. $fecha .'</td>'; 
						echo '<td>'.get_post_meta( get_the_id(), 'indice', true ).'</td>'; 
	                    echo '<td>'.get_post_meta( get_the_id(), 'minutos', true ).'</td>';
	                    $my_alerta =  get_post_meta( get_the_id(), 'alerta', true );
						echo '<td style="background: '.set_color_alerta($my_alerta).'">'.get_info_alerta($my_alerta).'</td>';
						echo '</tr>';
                    }
                }
                $content .= '</tbody>';
        }
        //Reset query
        wp_reset_query();
        return $content;
}
add_shortcode('mostrar_cat', 'show_category_posts');

// Add lightbox to all Images
add_filter('the_content', 'my_addlightboxrel');
function my_addlightboxrel($content) {
       global $post;
       $pattern ="/<a(.*?)href=('|\")(.*?).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>/i";
       $replacement = '<a$1href=$2$3.$4$5 rel="lightbox" title="'.$post->post_title.'"$6>';
       $content = preg_replace($pattern, $replacement, $content);
       return $content;
}
