��    b      ,  �   <      H  
   I     T     c     u     �  
   �  
   �     �  	   �     �  Y   �      	     $	     -	  +   6	     b	  	   j	     t	     |	     �	     �	  
   �	     �	  ?   �	     �	  5   �	     4
  
   9
     D
     I
     O
     ^
  	   n
  	   x
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
  E   �
     #     *  	   0  
   :     E     U      q     �     �     �     �     �          	  	             '  
   6     A     Q     ^     g  #   n     �     �     �     �  
   �  '   �                     ,     :     H     X  
   ^     i     n     w     �     �     �     �     �     �     �  	   �     �  8   �  +   &  *   R  #   }  �  �     :     H     Z     r     �     �     �  	   �     �     �  Y   �     0     6     ?  -   H     v  
   ~  	   �     �  
   �     �     �     �  Q   �        6   ;     r     x     �     �     �     �     �     �     �     �                    '     <     K     ]  V   e     �     �  	   �     �     �  #   �  -     +   H  $   t      �     �     �  
   �     �     �               ,     <     R     e  	   z  -   �     �     �     �     �     
  :        U     s     �     �     �     �     �     �  	   �  	             .     7     =     Q     g     w  	   �     �     �  B   �  /   �  *     $   >     9   -   <   O             B       K   %           >   (   Y   ;   Z   ^   '      A   N   H       b   &   _       V   +   ,         ?   /   C       [   $   6   8   \   `          :                                    5   D   P         .   L       I   ]   X   	   2          F   @          1          !             Q                             T   G   R   U   
   *       "       M       S                                  =   4   )       0         7   3       W                J   a   E   #    % Comments %1$s Page %2$s %s Theme Settings %s WordPress Theme %s ago (Untitled) 0 Comments 1 Column 1 Comment 404 Not Found <code>%1$s</code> &mdash; This function has been removed or replaced by another function. All Aperture Archives Are you sure you want to delete this field? Authors Bookmarks Browse: By %s Calendar Camera Categories Categorize? Comments are closed, but %strackbacks%s and pingbacks are open. Comments are closed. Copyright &#169; %1$s %2$s. Powered by %3$s and %4$s. Date Dimensions Edit Error Exclude admin? Featured Header File Name File Size File Type Focal Length Gallery Genre Hide empty? Hide invisible? Hierarchical? Home Layout Layout is a theme-specific structure for the single view of the post. Lyrics Merge Minute %s Navigation Navigation Menu Number of comments to show? Number of popular posts to show? Number of recent posts to show? Number of tags to show? One-letter abbreviation? Pad counts? Page %1$s of %2$s Page %s Pages Permalink Popular Posts Post Thumbnail Ravel Tabs Recent Comments Recent Posts Run Time Search Search results for &#8220;%s&#8221; Show count? Show description? Show full name? Show images? Show name? Show popular posts from last __ months: Show post count? Show private? Show rating? Show updated? Shutter Speed Skip to content Split Stylesheet Tags Template The main sidebar. Title: Track Type to search Use description? Visit Project Week %1$s of %2$s Week %s WordPress Year You are browsing the search results for &#8220;%s&#8221; You are browsing the site archives by time. You are browsing the site archives for %s. You are browsing the site archives. Project-Id-Version: Ravel
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-09-05 09:06+0000
PO-Revision-Date: 2014-09-05 09:54-0000
Last-Translator: Benito Anagua <benito.anagua@gmail.com>
Language-Team: Spanish <kde-i18n-doc@kde.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.7
Plural-Forms: nplurals=2; plural=(n != 1);
 % Comentarios %1$s Página %2$s Ajustes de plantilla %s Plantilla de Portal %s %s hace (Sin título) 0 Comentarios 1 columna 1 comentario 404 No Encontrado <code>%1$s</code> &mdash; Esta función ha sido eliminada o sustituido por otra función. Todos Apertura Archivos ¿Está seguro que desea eliminar este campo? Autores Marcadores Examinar: Por %s Calendario Cámara Categorías ¿Categorizar? Los comentarios están cerrados, pero %strackbacks%s y pingbacks están abiertos. Los comentarios están cerrados. Copyright &#169; %1$s %2$s. Impulsado por %3$s y %4$s. Fecha Dimensiones Editar Error ¿Excluir administrador? Cabecera recomendado Nombre de archivo Tamaño del archivo Tipo de archivo Longitud focal Galería Género ¿Ocultar vacía? ¿Ocultar invisible? ¿Jerárquico? Página de inicio Diseño El diseño es una estructura temática específica para la vista única de la entrada. Letras Fusión Minuto %s Navegación Menú de navegación ¿Número de comentarios a mostrar? ¿Número de entradas populares para mostrar? Número de entradas recientes para mostrar. ¿Número de etiquetas para mostrar? ¿Abreviatura de una sola letra? Recuentos Pad? Página %1$s de %2$s Página %s Páginas Enlaces permanentes  Entradas Populares Entrada Miniatura Pestañas Ravel Comentarios recientes Entradas recientes Tiempo de ejecución Búsqueda Resultados de búsqueda para &#8220;%s&#8221; Mostrar recuento? Mostrar una descripción? Mostrar nombre completo? Mostrar imágenes? Mostrar nombre? Mostrar las entradas más populares de las últimos meses: Mostrar recuento de entradas? Mostrar como privado? Mostrar clasificación? Mostrar actualizado? Velocidad de obturación Saltar al contenido Separación Hoja de estilo Etiquetas Plantilla La barra lateral principal. Título: Pista Escribe para buscar Utilice descripción? Visita proyecto Semana %1$s de %2$s Semana %s UAJMS Año Estás viendo los resultados de la búsqueda para &#8220;%s&#8221; Estás viendo los archivos de sitio por tiempo. Estás viendo los archivos de sitio de %s. Estás viendo los archivos de sitio. 