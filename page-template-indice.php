<?php 
/**
 * Template Name: Indice
 */

get_header(); // Loads the header.php template. ?>

<main <?php hybrid_attr( 'content' ); ?>>

	<?php if ( have_posts() ) : // Checks if any posts were found. ?>

		<?php while ( have_posts() ) : // Begins the loop through found posts. ?>

			<?php the_post(); // Loads the post data. ?>
				<?php  $content .= the_content(); ?>
				<?php //echo "<h3 align='center'>".get_the_content()."</h3>";?>

			<table width="100%" cellspacing="0" cellpadding="0" id="tabla">
			<thead>
			<tr>
			<th>Fecha</th>
			<th>Índice UV</th>
			<th>Minutos</th>
			<th>Alerta</th>
			</tr>
			</thead>
				<?php echo do_shortcode('[mostrar_cat cat="24"]'); ?>
			</table>
			<small><b>Minutos: </b>Tiempos máximos de exposición para evitar una quemadura en piel normal</small>
		<?php endwhile; // End found posts loop. ?>

	<?php endif; // End check for posts. ?>

</main><!-- #content -->

<?php get_footer(); // Loads the footer.php template. ?>

